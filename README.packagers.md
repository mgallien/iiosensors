<!---
# SPDX-FileCopyrightText: (c) 2020 Matthieu Gallien <matthieu_gallien@yahoo.fr>
#
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
-->

IIOSensors has the following dependencies:

-- REQUIRED:

 * Qt5Qml
 * Qt5Test
 * Qt5Core
 * Qt5 (required version >= 5.12.0)
 * ECM (required version >= 5.60.0)

Thanks in advance for your work.
