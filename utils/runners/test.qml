// SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

import QtQml
import QtQuick
import QtQuick.Controls
import org.kde.crelayMqtt

ApplicationWindow {
    title: "Test CRelay"
    visible: true
    minimumHeight: 400
    minimumWidth: 600

    CRelay {
        id: relay

        address: 'http://switch-dev:8000/'
        name: 'Dev Switch'

        onRelay1StateChanged:
            if (relay1State === CRelay.Active) {
                relay1Button.checked = true
            } else {
                relay1Button.checked = false
            }

        onRelay2StateChanged: console.log('relay 2 state: ' + relay2State)
    }

    RoundButton {
        id: relay1Button

        text: 'Relay 1'
        checkable: true
        enabled: relay.live

        onClicked:
        {
            if (relay.relay1State === CRelay.Active) {
                relay.relay1Command = CRelay.Inactive
            } else {
                relay.relay1Command = CRelay.Active
            }
        }

        width: 60
        height: 20
    }
}
