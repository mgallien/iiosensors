// SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

import QtQml
import org.kde.crelayMqtt
import org.kde.mqttqml

QtObject {
    id: parent

    default property list<QtObject> childs
    property string boardName: 'switch-dev'

    childs: [
        CRelay {
            id: relay

            address: 'http://' + parent.boardName + ':8000/'
            name: 'Dev Switch'

            onRelay1StateChanged: {
                console.log('relay 1 state: ' + relay1State)
                client.publishValue('actuator/' + boardName + '/relay1/position', relay1State)
            }

            onRelay2StateChanged: {
                console.log('relay 2 state: ' + relay2State)
                client.publishValue('actuator/' + boardName + '/relay2/position', relay2State)
            }
        },
        MqttClient {
            id: client
            hostname: 'sama5d3xplainedlight'
            port: {return 1883}

            onConnected: client.subscribe('actuator/' + boardName + '/#', subscriptionCommand)
        },
        QmlMqttSubscription {
            id: subscriptionCommand

            onMessageReceived: function (topic, msg) {
                console.log(topic, msg)
                if (topic === 'actuator/' + boardName + '/relay1/command') {
                    if (msg === '1') {
                        relay.setRelay1Command(CRelay.Active)
                    } else if (msg === '0') {
                        relay.setRelay1Command(CRelay.Inactive)
                    }
                } else if (topic === 'actuator/' + boardName + '/relay2/command') {
                    if (msg === '1') {
                        relay.setRelay2Command(CRelay.Active)
                    } else if (msg === '0') {
                        relay.setRelay2Command(CRelay.Inactive)
                    }
                }
            }
        }

    ]

    Component.onCompleted: {
        client.connectToHost()
    }
}
