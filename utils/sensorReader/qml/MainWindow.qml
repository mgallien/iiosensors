import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQml.Models 2.15

//import org.kde.kirigami 2.14 as Kirigami

import org.kde.iiosensors 1.0

ApplicationWindow {
    id: root

    width: 800
    height: 600

    title: 'Sensor Reader'
    visible: true

//    globalDrawer: Kirigami.GlobalDrawer {
//        title: root.title
//    }

    ListModel {
        id: modelData

        ListElement {
            name: "arietta-g25-128-1/temp"
            value: '3.25'
        }

        ListElement {
            name: "arietta-g25-128-2/temp"
            value: '3.25'
        }

        ListElement {
            name: "arietta-g25-128-3/temp"
            value: '3.25'
        }

        ListElement {
            name: "netsam9x25/temp"
            value: '3.25'
        }

        ListElement {
            name: "netsam9x25/temp1"
            value: '3.25'
        }

        ListElement {
            name: "netsam9x25/temp2"
            value: '3.25'
        }
    }

//    pageStack.initialPage: mainPageComponent

//    Component {
//        id: mainPageComponent

//        Kirigami.Page {
//            id: container

//            title: "Sensors"

//            }
//        }
//    }

    ScrollView {
        anchors.fill: parent

        ScrollView {
            anchors.fill: parent

            ListView {
                id: listView

                anchors.fill: parent

                Accessible.role: Accessible.List
                Accessible.name: root.title
                Accessible.description: root.title

                activeFocusOnTab: true
                keyNavigationEnabled: true

                reuseItems: true

                model: delegateModel
            }
        }
    }

    DelegateModel {
        id: delegateModel

        model: modelData

        delegate: ItemDelegate {
            height: 25
            width: listView.contentWidth
            text: model.name + ' ' + model.value
        }
    }

    ClientSubscription {
        id: clientSocket

        url: 'wss://data.mgallien.fr'
        topic: '#'
        version: ClientSubscription.MQTTv31

        onSocketConnected: {
            console.info('socket connected')
        }

        onSocketDisconnected: {
            console.info('socket disconnected')
            close()
            reconnectTimer.start()
        }

        onMessageReceived: function(topic, message) {
            console.info(new Date())
            console.info(topic, message)

            if (topic === 'netsam9x25/temp') {
                modelData.setProperty(3, 'value', message)
            } else if (topic === 'netsam9x25/temp1') {
                modelData.setProperty(4, 'value', message)
            } else if (topic === 'netsam9x25/temp2') {
                modelData.setProperty(5, 'value', message)
            } else if (topic === 'arietta-g25-128-1/temp') {
                modelData.setProperty(0, 'value', message)
            } else if (topic === 'arietta-g25-128-2/temp') {
                modelData.setProperty(1, 'value', message)
            } else if (topic === 'arietta-g25-128-3/temp') {
                modelData.setProperty(2, 'value', message)
            }
        }

        onPong: {
            pingPongTimer.pongReceived = true
        }
    }

    Timer {
        id: pingPongTimer

        property bool pingSent: false
        property bool pongReceived: false
        interval: 5000
        running: true

        onTriggered: {
            if (!pingSent) {
                clientSocket.ping()
                pingSent = true
                pongReceived = false
                start()
            } else {
                if (!pongReceived) {
                    console.info('closing socket due to missing pong message')
                    clientSocket.close()
                    pingSent = false
                    pongReceived = false
                } else {
                    pingSent = false
                    pongReceived = false
                    start()
                }
            }
        }
    }

    Timer {
      id: reconnectTimer

      interval: 1000

      onTriggered: clientSocket.connectAndSubscribe()
    }
}
