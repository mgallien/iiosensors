/*
   SPDX-FileCopyrightText: 2021 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

   SPDX-License-Identifier: LGPL-3.0-or-later
 */

//#define QT_QML_DEBUG

#include "config-sensor-reader.h"

//#include <KQuickAddons/QtQuickSettings>

//#include <KI18n/KLocalizedString>
//#include <KI18n/KLocalizedContext>

//#include <KCoreAddons/KAboutData>

#if defined KF5Crash_FOUND && KF5Crash_FOUND
#include <KCrash>
#endif

#include <QIcon>

#include <QApplication>
#include <QCommandLineParser>
#include <QStandardPaths>

#include <QQmlApplicationEngine>
#include <QJSEngine>
#include <QQmlFileSelector>
#include <QQuickStyle>
#include <QQmlContext>


#if defined Q_OS_ANDROID
int __attribute__((visibility("default"))) main(int argc, char *argv[])
#else
int main(int argc, char *argv[])
#endif
{
    QApplication app(argc, argv);

#if defined KF5Declarative_FOUND && KF5Declarative_FOUND
    KQuickAddons::QtQuickSettings::init();
#endif

//    KLocalizedString::setApplicationDomain("semsorReader");

#if defined KF5Crash_FOUND && KF5Crash_FOUND
    KCrash::initialize();
#endif

    QApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("semsorReader")));

//    KAboutData aboutData( QStringLiteral("semsorReader"),
//                          i18n("Sensor Reader"),
//                          QStringLiteral(")>)>!"),
//                          i18n("A Simple MQTT Frontend"),
//                          KAboutLicense::LGPL_V3,
//                          i18n("(c) 2020-2021, IIO Sensors contributors"));

//    aboutData.addAuthor(QStringLiteral("Matthieu Gallien"),i18n("Creator"), QStringLiteral("mgallien@mgallien.fr"));

//    KAboutData::setApplicationData(aboutData);

    QCommandLineParser parser;
//    aboutData.setupCommandLine(&parser);
    parser.process(app);
//    aboutData.processCommandLine(&parser);

#if defined Q_OS_ANDROID
    QQuickStyle::setStyle(QStringLiteral("Material"));
#else
//    QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
    QQuickStyle::setFallbackStyle(QStringLiteral("Fusion"));
#endif

    QQmlApplicationEngine engine;
    engine.addImportPath(QStringLiteral(":/"));
    engine.addImportPath(QStringLiteral("qrc:/imports"));
    QQmlFileSelector selector(&engine);

//    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));

    engine.load(QUrl(QStringLiteral("qrc:/qml/MainWindow.qml")));

    return app.exec();
}
