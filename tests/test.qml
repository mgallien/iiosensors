// SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>
//
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

import QtQml 2.15
import org.kde.iiosensors 1.0

QtObject {
    default property list<QtObject> childs
    property string boardName: 'netsam9x25'

    childs: [
        SensorDevice {
            id: device

            filePath: '/sys/class/i2c-dev/i2c-0/device/0-0076/iio:device2'
        },
        TemperatureSensorChannel {
            id: tempChannel

            filePath: device.filePath
            deviceType: device.type
            name: 'temp'

            onValueChanged: {
                console.log(name + ' channel value ' + value)
                client.publishValue(boardName + '/' + name, value)
            }
        },
        PressureSensorChannel {
            id: pressureChannel

            filePath: device.filePath
            deviceType: device.type
            name: 'pressure'

            onValueChanged: {
                console.log(name + ' channel value ' + value)
                client.publishValue(boardName + '/' + name, value)
            }
        },
        HumidityRelativeChannel {
            id: humidityRelativeChannel

            filePath: device.filePath
            deviceType: device.type
            name: 'humidityrelative'

            onValueChanged: {
                console.log(name + ' channel value ' + value)
                client.publishValue(boardName + '/' + name, value)
            }
        },
        Timer {
            running: true
            repeat: true
            interval: 300000

            onTriggered: {
                tempChannel.refreshValue()
                pressureChannel.refreshValue()
                humidityRelativeChannel.refreshValue()
            }
        },
        MqttClient {
            id: client
            hostname: 'sama5d3xplainedlight'
            networkPort: 1883
        }
    ]

    Component.onCompleted: {
        client.connectToHost()
        console.log('sensor name: ' + device.name + ' ' + device.availableChannels)
    }
}
