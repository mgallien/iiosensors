/**
  SPDX-FileCopyrightText: 2022 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef CRELAY_H
#define CRELAY_H

#include <QObject>
#include <QString>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QTimer>
#include <qqmlregistration.h>

class CRelay : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

    Q_PROPERTY(QUrl address READ address WRITE setAddress NOTIFY addressChanged)

    Q_PROPERTY(bool live READ live NOTIFY liveChanged)

    Q_PROPERTY(RelayState relay1State READ relay1State NOTIFY relay1StateChanged)

    Q_PROPERTY(RelayState relay1Command READ relay1Command WRITE setRelay1Command NOTIFY relay1CommandChanged)

    Q_PROPERTY(RelayState relay2State READ relay2State NOTIFY relay2StateChanged)

    Q_PROPERTY(RelayState relay2Command READ relay2Command WRITE setRelay2Command NOTIFY relay2CommandChanged)

public:

    enum class RelayState {
        Active = 1,
        Inactive = 0,
    };

    Q_ENUM(RelayState)

    explicit CRelay(QObject *parent = nullptr);

    ~CRelay() override;

    [[nodiscard]] QString name() const;

    [[nodiscard]] QUrl address() const;

    [[nodiscard]] bool live() const;

    [[nodiscard]] RelayState relay1State() const;

    [[nodiscard]] RelayState relay1Command() const;

    [[nodiscard]] RelayState relay2State() const;

    [[nodiscard]] RelayState relay2Command() const;

Q_SIGNALS:

    void nameChanged();

    void addressChanged();

    void liveChanged();

    void relay1StateChanged();

    void relay1CommandChanged();

    void relay2StateChanged();

    void relay2CommandChanged();

public Q_SLOTS:

    void setName(const QString &newName);

    void setAddress(const QUrl &newAddress);

    void setRelay1Command(CRelay::RelayState newRelay1Command);

    void setRelay2Command(CRelay::RelayState newRelay2Command);

private Q_SLOTS:

    void queryRemoteState();

    void setRemoteState(int relayIndex, RelayState desiredState);

private:

    void startMonitoring();

    void sendRelayQuery(QNetworkRequest apiRequest);

    QString mName;

    QUrl mAddress;

    bool mLive = false;

    QTimer mPingTimer;

    QNetworkAccessManager mNetworkManager;

    RelayState mRelay1State = CRelay::RelayState::Inactive;

    RelayState mRelay1Command = CRelay::RelayState::Inactive;

    RelayState mRelay2State = CRelay::RelayState::Inactive;

    RelayState mRelay2Command = CRelay::RelayState::Inactive;
};

#endif
