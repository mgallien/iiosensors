/**
  SPDX-FileCopyrightText: 2022 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "crelay.h"

#include "crelayMqttLogging.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>

CRelay::CRelay(QObject *parent) : QObject(parent)
{
    connect(&mPingTimer, &QTimer::timeout, this, &CRelay::queryRemoteState);
}

CRelay::~CRelay() = default;

QString CRelay::name() const
{
    return mName;
}

void CRelay::setName(const QString &newName)
{
    if (mName == newName) {
        return;
    }

    mName = newName;
    Q_EMIT nameChanged();
}

QUrl CRelay::address() const
{
    return mAddress;
}

bool CRelay::live() const
{
    return mLive;
}

void CRelay::setAddress(const QUrl &newAddress)
{
    if (mAddress == newAddress) {
        return;
    }

    mAddress = newAddress;
    Q_EMIT addressChanged();

    startMonitoring();
    queryRemoteState();
}

void CRelay::queryRemoteState()
{
    auto apiUrl = mAddress;
    apiUrl.setPath(QStringLiteral("/gpio"));
    auto apiRequest = QNetworkRequest{apiUrl};

    sendRelayQuery(apiRequest);
}

void CRelay::setRemoteState(int relayIndex, RelayState desiredState)
{
    auto apiUrl = mAddress;
    apiUrl.setPath(QStringLiteral("/gpio"));
    auto urlQuery = QUrlQuery{};

    urlQuery.addQueryItem(QStringLiteral("pin"), QString::number(relayIndex));
    urlQuery.addQueryItem(QStringLiteral("status"), desiredState == RelayState::Active ? QStringLiteral("1") : QStringLiteral("0"));

    apiUrl.setQuery(urlQuery);
    auto apiRequest = QNetworkRequest{apiUrl};

    sendRelayQuery(apiRequest);
}

void CRelay::startMonitoring()
{
    qCInfo(orgKdeCrelayMqtt()) << "startMonitoring" << mAddress;

    constexpr auto timerInterval = 2000;
    mPingTimer.setInterval(timerInterval);
    mPingTimer.setSingleShot(false);
    mPingTimer.start();
}

void CRelay::sendRelayQuery(QNetworkRequest apiRequest)
{
    qCInfo(orgKdeCrelayMqtt()) << "get request" << apiRequest.url();

    auto reply = mNetworkManager.get(apiRequest);

    connect(reply, &QNetworkReply::errorOccurred, reply, [] (QNetworkReply::NetworkError error) {
        qCInfo(orgKdeCrelayMqtt()) << "errorOccurred" << error;
    });
    connect(reply, &QNetworkReply::finished, reply, [reply, this] () {
        reply->deleteLater();

        if (reply->error() != QNetworkReply::NoError) {
            mLive = false;
            Q_EMIT liveChanged();
            return;
        } else {
            mLive = true;
            Q_EMIT liveChanged();
        }

        const auto replyData = QString::fromLatin1(reply->readAll());
        auto replyText = QStringView{replyData};
        auto relayStates = replyText.split(QStringView(u"<br>"), Qt::SkipEmptyParts);

        for(const auto oneRelay : relayStates) {
            auto relayData = oneRelay.split(u':');

            if (relayData.size() != 2) {
                continue;
            }

            if (relayData.front() == u"Relay 1") {
                if (relayData.back() == u"1" && mRelay1State != RelayState::Active) {
                    mRelay1State = RelayState::Active;
                    Q_EMIT relay1StateChanged();
                } else if (relayData.back() == u"0" && mRelay1State != RelayState::Inactive) {
                    mRelay1State = RelayState::Inactive;
                    Q_EMIT relay1StateChanged();
                }

                if (mRelay1Command != mRelay1State) {
                    mRelay1Command = mRelay1State;
                    Q_EMIT relay1CommandChanged();
                }
            } else if (relayData.front() == u"Relay 2") {
                if (relayData.back() == u"1" && mRelay2State != RelayState::Active) {
                    mRelay2State = RelayState::Active;
                    Q_EMIT relay2StateChanged();
                } else if (relayData.back() == u"0" && mRelay2State != RelayState::Inactive) {
                    mRelay2State = RelayState::Inactive;
                    Q_EMIT relay2StateChanged();
                }

                if (mRelay2Command != mRelay2State) {
                    mRelay2Command = mRelay2State;
                    Q_EMIT relay2CommandChanged();
                }
            }
        }
    });
}

CRelay::RelayState CRelay::relay1State() const
{
    return mRelay1State;
}

CRelay::RelayState CRelay::relay1Command() const
{
    return mRelay1Command;
}

void CRelay::setRelay1Command(CRelay::RelayState newRelay1Command)
{
    if (mRelay1Command == newRelay1Command) {
        return;
    }

    mRelay1Command = newRelay1Command;
    Q_EMIT relay1CommandChanged();

    setRemoteState(1, mRelay1Command);
}

CRelay::RelayState CRelay::relay2State() const
{
    return mRelay2State;
}

CRelay::RelayState CRelay::relay2Command() const
{
    return mRelay2Command;
}

void CRelay::setRelay2Command(CRelay::RelayState newRelay2Command)
{
    if (mRelay2Command == newRelay2Command) {
        return;
    }

    mRelay2Command = newRelay2Command;
    Q_EMIT relay2CommandChanged();

    setRemoteState(2, mRelay2Command);
}


#include "moc_crelay.cpp"
