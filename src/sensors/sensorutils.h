/**
  SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef SENSORUTILS_H
#define SENSORUTILS_H

#include <QMetaType>

namespace IIOSensors
{
Q_NAMESPACE

enum class DeviceKind {
    LinuxIIODevice,
    LinuxHwmonDevice,
    UnknownDevice,
};

Q_ENUM_NS(DeviceKind)

}

#endif // SENSORUTILS_H
