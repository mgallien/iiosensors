/**
  SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "sensordevice.h"

#include "sensorutils.h"

#include "iioSensorsLogging.h"

#include <QDir>
#include <QFile>
#include <QRegularExpression>

class SensorDevicePrivate
{
public:
    QString mName;

    QString mFilePath;

    QStringList mAvailableChannels;

    IIOSensors::DeviceKind mDeviceKind = IIOSensors::DeviceKind::UnknownDevice;

    bool mIsValid = false;
};

SensorDevice::SensorDevice(QObject *parent)
    : QObject(parent)
    , d(std::make_unique<SensorDevicePrivate>())
{
}

SensorDevice::~SensorDevice() = default;

const QString &SensorDevice::name() const
{
    return d->mName;
}

const QString &SensorDevice::filePath() const
{
    return d->mFilePath;
}

bool SensorDevice::isValid() const
{
    return d->mIsValid;
}

IIOSensors::DeviceKind SensorDevice::type() const
{
    return d->mDeviceKind;
}

void SensorDevice::setFilePath(QString filePath)
{
    if (d->mFilePath == filePath) {
        return;
    }

    d->mFilePath = std::move(filePath);
    Q_EMIT filePathChanged();

    initialize();
}

const QStringList &SensorDevice::availableChannels() const
{
    return d->mAvailableChannels;
}

void SensorDevice::initialize()
{
    if (d->mFilePath.isEmpty()) {
        qCInfo(orgKdeIIOSensors()) << "filePath is empty";
        return;
    }

    QFile nameFile = {d->mFilePath + QStringLiteral("/name")};
    if (!nameFile.open(QIODevice::ReadOnly)) {
        qCInfo(orgKdeIIOSensors()) << d->mFilePath << "cannot be opened";
        return;
    }

    auto rawData = nameFile.readAll();
    d->mName = QString::fromLatin1(rawData.left(rawData.size() - 1));
    Q_EMIT nameChanged();

    d->mIsValid = !d->mName.isEmpty();
    Q_EMIT isValidChanged();

    d->mAvailableChannels.clear();
    QDir sensorDirectory {d->mFilePath};

    if (d->mDeviceKind == IIOSensors::DeviceKind::UnknownDevice) {
        static QRegularExpression iioSensorName {QStringLiteral("^in_(.*)_input$")};
        auto iioChannelNames = sensorDirectory.entryList({QStringLiteral("in_*_input")});
        for (const auto &channelName : iioChannelNames) {
            auto result = iioSensorName.match(channelName);
            if (result.hasMatch()) {
                qCInfo(orgKdeIIOSensors()) << d->mName << "channel" << result.captured(1);
                d->mAvailableChannels.push_back(result.captured(1));
                d->mDeviceKind = IIOSensors::DeviceKind::LinuxIIODevice;
            } else {
                qCInfo(orgKdeIIOSensors()) << d->mName << "not a channel" << channelName;
            }
        }
    }

    if (d->mDeviceKind == IIOSensors::DeviceKind::UnknownDevice) {
        static QRegularExpression hwmonSensorName {QStringLiteral("^(.*)_input$")};
        auto hwmonChannelNames = sensorDirectory.entryList({QStringLiteral("*_input")});
        for (const auto &channelName : hwmonChannelNames) {
            auto result = hwmonSensorName.match(channelName);
            if (result.hasMatch()) {
                qCInfo(orgKdeIIOSensors()) << d->mName << "channel" << result.captured(1);
                d->mAvailableChannels.push_back(result.captured(1));
                d->mDeviceKind = IIOSensors::DeviceKind::LinuxHwmonDevice;
            } else {
                qCInfo(orgKdeIIOSensors()) << d->mName << "not a channel" << channelName;
            }
        }
    }

    Q_EMIT availableChannelsChanged();
}

#include "moc_sensordevice.cpp"
