/**
  SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef SENSORCHANNEL_H
#define SENSORCHANNEL_H

#include "sensorutils.h"

#include <QObject>
#include <qqmlregistration.h>

#include <memory>

class SensorChannelPrivate;

class SensorChannel : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)
    Q_PROPERTY(QString suffix READ suffix WRITE setSuffix NOTIFY suffixChanged)
    Q_PROPERTY(IIOSensors::DeviceKind deviceType READ deviceType WRITE setDeviceType NOTIFY deviceTypeChanged)
    Q_PROPERTY(bool isValid READ isValid NOTIFY isValidChanged)
    Q_PROPERTY(double value READ value NOTIFY valueChanged)

public:
    explicit SensorChannel(QObject *parent = nullptr);

    ~SensorChannel() override;

    [[nodiscard]] const QString &name() const;

    [[nodiscard]] const QString &filePath() const;

    [[nodiscard]] IIOSensors::DeviceKind deviceType() const;

    [[nodiscard]] bool isValid() const;

    [[nodiscard]] double value() const;

    [[nodiscard]] QString suffix() const;

public Q_SLOTS:
    void setFilePath(QString filePath);

    void setName(QString name);

    void setDeviceType(IIOSensors::DeviceKind deviceType);

    void refreshValue();

    void setSuffix(QString suffix);

protected:
    void setValue(double newValue);

    virtual void updateValue(bool validValue, double newValue);

Q_SIGNALS:
    void nameChanged();

    void filePathChanged();

    void suffixChanged();

    void deviceTypeChanged();

    void isValidChanged();

    void valueChanged();

private:
    void initialize();

    std::unique_ptr<SensorChannelPrivate> d;
};

#endif // SENSORCHANNEL_H
