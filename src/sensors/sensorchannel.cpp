/**
  SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "sensorchannel.h"

#include "iioSensorsLogging.h"

#include <QFile>

class SensorChannelPrivate
{
public:
    QString mName;

    QString mFilePath;

    QString mSuffix = QStringLiteral("_input");

    IIOSensors::DeviceKind mDeviceType = IIOSensors::DeviceKind::UnknownDevice;

    bool mIsValid = false;

    QFile mInputFile;

    double mCurrentValue = 0.;
};

SensorChannel::SensorChannel(QObject *parent)
    : QObject(parent)
    , d(std::make_unique<SensorChannelPrivate>())
{
}

IIOSensors::DeviceKind SensorChannel::deviceType() const
{
    return d->mDeviceType;
}

SensorChannel::~SensorChannel() = default;

const QString &SensorChannel::name() const
{
    return d->mName;
}

const QString &SensorChannel::filePath() const
{
    return d->mFilePath;
}

bool SensorChannel::isValid() const
{
    return d->mIsValid;
}

double SensorChannel::value() const
{
    return d->mCurrentValue;
}

QString SensorChannel::suffix() const
{
    return d->mSuffix;
}

void SensorChannel::setFilePath(QString filePath)
{
    if (d->mFilePath == filePath) {
        return;
    }

    d->mFilePath = std::move(filePath);
    Q_EMIT filePathChanged();

    initialize();
}

void SensorChannel::setName(QString name)
{
    if (d->mName == name) {
        return;
    }

    d->mName = std::move(name);
    Q_EMIT nameChanged();

    initialize();
}

void SensorChannel::setDeviceType(IIOSensors::DeviceKind deviceType)
{
    if (d->mDeviceType == deviceType) {
        return;
    }

    d->mDeviceType = std::move(deviceType);
    Q_EMIT deviceTypeChanged();

    initialize();
}

void SensorChannel::refreshValue()
{
    d->mInputFile.seek(0);
    auto rawData = d->mInputFile.readAll();
    auto newRawValue = QString::fromLatin1(rawData.left(rawData.size() - 1));

    bool validValue = false;
    auto newValue = newRawValue.toDouble(&validValue);

    qCDebug(orgKdeIIOSensors()) << "SensorChannel::refreshValue" << newRawValue << validValue << newValue;

    updateValue(validValue, newValue);
}

void SensorChannel::setSuffix(QString suffix)
{
    if (d->mSuffix == suffix) {
        return;
    }

    d->mSuffix = suffix;
    Q_EMIT suffixChanged();
}

void SensorChannel::setValue(double newValue)
{
    d->mCurrentValue = newValue;
}

void SensorChannel::updateValue(bool validValue, double newValue)
{
    if (validValue && abs(d->mCurrentValue - newValue) > 0.1) {
        d->mCurrentValue = newValue;
        Q_EMIT valueChanged();
    }
}

void SensorChannel::initialize()
{
    if (d->mFilePath.isEmpty()) {
        qCDebug(orgKdeIIOSensors()) << "SensorChannel::initialize"
                                    << "filePath is empty";
        return;
    }

    if (d->mName.isEmpty()) {
        qCDebug(orgKdeIIOSensors()) << "SensorChannel::initialize"
                                    << "name is empty";
        return;
    }

    if (d->mDeviceType == IIOSensors::DeviceKind::UnknownDevice) {
        qCDebug(orgKdeIIOSensors()) << "SensorChannel::initialize"
                                    << "deviceType is unknown";
        return;
    }

    QString valueFileNamePrefix;
    switch (d->mDeviceType) {
    case IIOSensors::DeviceKind::LinuxIIODevice:
        valueFileNamePrefix = QStringLiteral("in_");
        break;
    case IIOSensors::DeviceKind::LinuxHwmonDevice:
        break;
    case IIOSensors::DeviceKind::UnknownDevice:
        break;
    }

    d->mInputFile.setFileName(d->mFilePath + QStringLiteral("/") + valueFileNamePrefix + d->mName + d->mSuffix);

    if (!d->mInputFile.open(QIODevice::ReadOnly)) {
        qCInfo(orgKdeIIOSensors()) << d->mFilePath << "cannot be opened";
        return;
    }

    d->mIsValid = true;
    Q_EMIT isValidChanged();

    refreshValue();
}

#include "moc_sensorchannel.cpp"
