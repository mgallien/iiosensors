#ifndef SENSORMODEL_H
#define SENSORMODEL_H

#include <QAbstractListModel>
#include <qqmlregistration.h>

#include <memory>

class SensorModelPrivate;

class SensorModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

public:
    explicit SensorModel(QObject *parent = nullptr);

    ~SensorModel() override;

    [[nodiscard]] int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    [[nodiscard]] QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    [[nodiscard]] QHash<int, QByteArray> roleNames() const override;

public Q_SLOTS:
    void messageReceived(QString topic, QString message);

private:
    std::unique_ptr<SensorModelPrivate> d;
};

#endif // SENSORMODEL_H
