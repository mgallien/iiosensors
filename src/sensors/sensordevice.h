/**
  SPDX-FileCopyrightText: 2020 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef SENSORDEVICE_H
#define SENSORDEVICE_H

#include "sensorutils.h"

#include <QObject>
#include <qqmlregistration.h>

#include <memory>

class SensorDevicePrivate;

class SensorDevice : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)
    Q_PROPERTY(IIOSensors::DeviceKind type READ type NOTIFY typeChanged)
    Q_PROPERTY(bool isValid READ isValid NOTIFY isValidChanged)
    Q_PROPERTY(QStringList availableChannels READ availableChannels NOTIFY availableChannelsChanged)

public:
    explicit SensorDevice(QObject *parent = nullptr);

    ~SensorDevice() override;

    [[nodiscard]] const QString &name() const;

    [[nodiscard]] const QString &filePath() const;

    [[nodiscard]] IIOSensors::DeviceKind type() const;

    [[nodiscard]] bool isValid() const;

    [[nodiscard]] const QStringList &availableChannels() const;

public Q_SLOTS:

    void setFilePath(QString filePath);

Q_SIGNALS:

    void nameChanged();

    void filePathChanged();

    void typeChanged();

    void isValidChanged();

    void availableChannelsChanged();

private:
    void initialize();

    std::unique_ptr<SensorDevicePrivate> d;
};

#endif
