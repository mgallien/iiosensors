#include "humidityrelativechannel.h"

#include "iioSensorsLogging.h"

HumidityRelativeChannel::HumidityRelativeChannel(QObject *parent)
    : SensorChannel(parent)
{
}

void HumidityRelativeChannel::updateValue(bool validValue, double newValue)
{
    constexpr auto SCALE_FACTOR = 1000.;
    newValue = newValue / SCALE_FACTOR;

    qCDebug(orgKdeIIOSensors()) << "HumidityRelativeChannel::updateValue" << validValue << newValue << value();

    if (validValue) {
        setValue(newValue);
        Q_EMIT valueChanged();
    }
}

#include "moc_humidityrelativechannel.cpp"
