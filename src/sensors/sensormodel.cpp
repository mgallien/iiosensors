#include "sensormodel.h"

#include <QVector>
#include <QHash>

class SensorModelPrivate
{
public:

    QVector<QString> mIndex;

    QHash<QString, double> mData;
};

SensorModel::SensorModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

SensorModel::~SensorModel() = default;

int SensorModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return 0;
}

QVariant SensorModel::data([[maybe_unused]] const QModelIndex &index, [[maybe_unused]] int role) const
{
    auto result = QVariant{};

    return result;
}

QHash<int, QByteArray> SensorModel::roleNames() const
{
    return QAbstractListModel::roleNames();
}

void SensorModel::messageReceived([[maybe_unused]] QString topic, [[maybe_unused]] QString message)
{

}
