/**
  SPDX-FileCopyrightText: 2021 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef BOARDINFO_H
#define BOARDINFO_H

#include <QObject>
#include <qqmlregistration.h>

#include <memory>

class BoardInfoPrivate;

class BoardInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString boardName READ boardName NOTIFY boardNameChanged)
public:
    explicit BoardInfo(QObject *parent = nullptr);

    const QString &boardName() const;

Q_SIGNALS:

    void boardNameChanged();

private:
    std::unique_ptr<BoardInfoPrivate> d;
};

#endif // BOARDINFO_H
