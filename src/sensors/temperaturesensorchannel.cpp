#include "temperaturesensorchannel.h"

#include "iioSensorsLogging.h"

TemperatureSensorChannel::TemperatureSensorChannel(QObject *parent)
    : SensorChannel(parent)
{
}

void TemperatureSensorChannel::updateValue(bool validValue, double newValue)
{
    newValue = newValue / 1000.;

    qCDebug(orgKdeIIOSensors()) << "TemperatureSensorChannel::updateValue" << validValue << newValue << value();

    if (validValue) {
        setValue(newValue);
        Q_EMIT valueChanged();
    }
}

#include "moc_temperaturesensorchannel.cpp"
