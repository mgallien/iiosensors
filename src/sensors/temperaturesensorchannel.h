#ifndef TEMPERATURESENSORCHANNEL_H
#define TEMPERATURESENSORCHANNEL_H

#include "sensorchannel.h"

#include <qqmlregistration.h>

class TemperatureSensorChannel : public SensorChannel
{
    Q_OBJECT
    QML_ELEMENT

public:
    TemperatureSensorChannel(QObject *parent = nullptr);

protected:
    void updateValue(bool validValue, double newValue) override;
};

#endif // TEMPERATURESENSORCHANNEL_H
