#ifndef RELATIVEHUMIDITYCHANNEL_H
#define RELATIVEHUMIDITYCHANNEL_H

#include "sensorchannel.h"

#include <qqmlregistration.h>

class HumidityRelativeChannel : public SensorChannel
{
    Q_OBJECT
    QML_ELEMENT

public:
    HumidityRelativeChannel(QObject *parent = nullptr);

protected:
    void updateValue(bool validValue, double newValue) override;
};

#endif // RELATIVEHUMIDITYCHANNEL_H
