/**
  SPDX-FileCopyrightText: 2021 (c) Matthieu Gallien <matthieu_gallien@yahoo.fr>

  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "boardinfo.h"

#include <QHostInfo>

class BoardInfoPrivate
{
public:
    QString mBoardName;
};

BoardInfo::BoardInfo(QObject *parent)
    : QObject(parent)
    , d(std::make_unique<BoardInfoPrivate>())
{
    d->mBoardName = QHostInfo::localHostName();
}

const QString &BoardInfo::boardName() const
{
    return d->mBoardName;
}

#include "moc_boardinfo.cpp"
