#include "pressuresensorchannel.h"

#include "iioSensorsLogging.h"

PressureSensorChannel::PressureSensorChannel(QObject *parent)
    : SensorChannel(parent)
{
}

void PressureSensorChannel::updateValue(bool validValue, double newValue)
{
    constexpr auto SCALE_FACTOR = 10.;
    newValue = newValue * SCALE_FACTOR;

    qCDebug(orgKdeIIOSensors()) << "PressureSensorChannel::updateValue" << validValue << newValue << value();

    if (validValue) {
        setValue(newValue);
        Q_EMIT valueChanged();
    }
}

#include "moc_pressuresensorchannel.cpp"
