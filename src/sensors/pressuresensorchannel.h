#ifndef PRESSURESENSORCHANNEL_H
#define PRESSURESENSORCHANNEL_H

#include "sensorchannel.h"

#include <qqmlregistration.h>

class PressureSensorChannel : public SensorChannel
{
    Q_OBJECT
    QML_ELEMENT

public:
    PressureSensorChannel(QObject *parent = nullptr);

protected:
    void updateValue(bool validValue, double newValue) override;
};

#endif // PRESSURESENSORCHANNEL_H
