/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "websocketmqttclient.h"

#include <QMqttClient>
#include "QMqttMessage"
#include "QMqttPublishProperties"

#include <QtCore/QCoreApplication>
#include <QtCore/QLoggingCategory>


Q_LOGGING_CATEGORY(lcWebSocketMqtt, "org.kde.mqtt.websocket")

WebSocketMqttClient::WebSocketMqttClient(QObject *parent) : QObject(parent)
{
    connect(&m_device, &WebSocketIODevice::socketConnected,
            this, &WebSocketMqttClient::socketConnected);
    connect(&m_device, &WebSocketIODevice::socketDisconnected,
            this, &WebSocketMqttClient::socketDisconnected);
    connect(&m_device, &WebSocketIODevice::error,
            this, &WebSocketMqttClient::socketError);
    connect(&m_device, &WebSocketIODevice::pong,
            this, &WebSocketMqttClient::pong);

    connect(&m_device, &WebSocketIODevice::socketConnected, this, [this]() {
        qCDebug(lcWebSocketMqtt) << "WebSocket connected, initializing MQTT connection.";

        switch (m_version)
        {
        case MQTT_VERSION::MQTTv31:
            m_client.setProtocolVersion(QMqttClient::MQTT_3_1);
            break;
        case MQTT_VERSION::MQTTv311:
            m_client.setProtocolVersion(QMqttClient::MQTT_3_1_1);
            break;
        case MQTT_VERSION::MQTTv5:
            m_client.setProtocolVersion(QMqttClient::MQTT_5_0);
            break;
        }

        m_client.setTransport(&m_device, QMqttClient::IODevice);

        m_client.connectToHost();
    });

    connect(&m_client, &QMqttClient::connected, this, [this]() {
        qCDebug(lcWebSocketMqtt) << "MQTT connection established";

        m_subscription = m_client.subscribe(m_topic);
        if (!m_subscription) {
            qDebug() << "Failed to subscribe to " << m_topic;
            Q_EMIT mqttErrorOccured();
        }

        connect(m_subscription, &QMqttSubscription::stateChanged,
                [](QMqttSubscription::SubscriptionState s) {
            qCDebug(lcWebSocketMqtt) << "Subscription state changed:" << s;
        });

        connect(m_subscription, &QMqttSubscription::messageReceived, m_subscription,
                [this](QMqttMessage msg) {
            switch (msg.publishProperties().payloadFormatIndicator())
            {
            case QMqtt::PayloadFormatIndicator::UTF8Encoded:
                Q_EMIT messageReceived(msg.topic().name(), QString::fromUtf8(msg.payload()));
                break;
            case QMqtt::PayloadFormatIndicator::Unspecified:
                Q_EMIT messageReceived(msg.topic().name(), QString::fromLatin1(msg.payload()));
                break;
            }
        });
    });
}

void WebSocketMqttClient::classBegin()
{
}

void WebSocketMqttClient::componentComplete()
{
    if (m_url.isValid()) {
        connectAndSubscribe();
    }
}

void WebSocketMqttClient::setUrl(const QUrl &url)
{
    if (m_url == url) {
        return;
    }

    m_url = url;
    Q_EMIT urlChanged();
}

void WebSocketMqttClient::setTopic(const QString &topic)
{
    if (m_topic == topic) {
        return;
    }

    m_topic = topic;
    Q_EMIT topicChanged();
}

void WebSocketMqttClient::setVersion(MQTT_VERSION version)
{
    if (m_version == version) {
        return;
    }

    m_version = version;
    Q_EMIT versionChanged();
}

void WebSocketMqttClient::ping(const QByteArray &payload)
{
    m_device.ping(payload);
}

void WebSocketMqttClient::close()
{
    m_client.disconnectFromHost();
    m_device.close();
    delete m_subscription;
    m_subscription = nullptr;
}

void WebSocketMqttClient::connectAndSubscribe()
{
    qCDebug(lcWebSocketMqtt) << "Connecting to broker at " << m_url;

    m_device.setUrl(m_url);

    switch (m_version)
    {
    case MQTT_VERSION::MQTTv31:
        m_device.setProtocol("mqttv3.1");
        break;
    case MQTT_VERSION::MQTTv311:
        m_device.setProtocol("mqttv3.1.1");
        break;
    case MQTT_VERSION::MQTTv5:
        m_device.setProtocol("mqttv5.0");
        break;
    }

    if (!m_device.open(QIODevice::ReadWrite)) {
        qDebug() << "Could not open socket device";
    }
}

#include "moc_websocketmqttclient.cpp"
