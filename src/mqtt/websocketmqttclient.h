/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#ifndef WEBSOCKETMQTTCLIENT_H
#define WEBSOCKETMQTTCLIENT_H

#include "websocketiodevice.h"

#include <QObject>
#include <QMqttClient>
#include <QWebSocket>
#include <QQmlParserStatus>
#include <qqmlregistration.h>

class WebSocketMqttClient : public QObject, public QQmlParserStatus
{
    Q_OBJECT
    QML_ELEMENT
    Q_INTERFACES(QQmlParserStatus)

    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString topic READ topic WRITE setTopic NOTIFY topicChanged)
    Q_PROPERTY(MQTT_VERSION version READ version WRITE setVersion NOTIFY versionChanged)
public:
    enum class MQTT_VERSION {
        MQTTv31,
        MQTTv311,
        MQTTv5,
    };

    Q_ENUM(MQTT_VERSION)

    WebSocketMqttClient(QObject *parent = nullptr);

    [[nodiscard]] QUrl url() const
    {
        return m_url;
    }

    [[nodiscard]] QString topic() const
    {
        return m_topic;
    }

    [[nodiscard]] MQTT_VERSION version() const
    {
        return m_version;
    }

    void classBegin() override;

    void componentComplete() override;

Q_SIGNALS:
    void socketConnected();

    void socketDisconnected();

    void socketError(QAbstractSocket::SocketError error);

    void pong(quint64 elapsedTime, const QByteArray &payload);

    void messageReceived(QString topic, QString message);

    void mqttErrorOccured();

    void urlChanged();

    void topicChanged();

    void versionChanged();

public Q_SLOTS:
    void close();

    void connectAndSubscribe();

    void setUrl(const QUrl &url);

    void setTopic(const QString &topic);

    void setVersion(WebSocketMqttClient::MQTT_VERSION version);

    void ping(const QByteArray &payload = {});

private:
    QMqttClient m_client;
    QMqttSubscription *m_subscription = nullptr;
    QUrl m_url;
    QString m_topic;
    WebSocketIODevice m_device;
    MQTT_VERSION m_version = MQTT_VERSION::MQTTv31;
};



#endif // WEBSOCKETMQTTCLIENT_H

