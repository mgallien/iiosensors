<!---
# SPDX-FileCopyrightText: (c) 2020 Matthieu Gallien <matthieu_gallien@yahoo.fr>
#
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
-->

# IIO Sensors

This library should allow easy access to sensors provided by IIO and hwmon Linux subsystems for usage in embedded systems

